# Computer Architecture #
Repository for Computer Architecture

BitBucket Repository: [https://bitbucket.org/](https://bitbucket.org/MarioP-Dev/2ac-pl2-uo283720/src/master/)
## Index
 - [x] [Session 1-1](https://bitbucket.org/MarioP-Dev/2ac-pl2-uo283720/src/master/session1-1/)
 - [x] [Session 1-2](https://bitbucket.org/MarioP-Dev/2ac-pl2-uo283720/src/master/session1-2/)
 - [x] [Session 1-3](https://bitbucket.org/MarioP-Dev/2ac-pl2-uo283720/src/master/session1-3/)
 - [x] [Session 2-4](https://bitbucket.org/MarioP-Dev/2ac-pl2-uo283720/src/master/session2-4/)
 - [_] [Session 3-3](https://bitbucket.org/MarioP-Dev/2ac-pl2-uo283720/src/master/session3-3/)
 - [_] [Session 3-4](https://bitbucket.org/MarioP-Dev/2ac-pl2-uo283720/src/master/session3-4/)

## About
![Mario Pérez](https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/61436f93c2fda2006a8be521/1f789ab3-16b3-4951-971e-6839bb5bc85b/128?s=128)
### Mario Pérez
[GitHub](https://github.com/MarioP-Dev)
[Webpage](https://mariopdev.com)
[Email](mailto:mariosoiram1@gmail.com)
[BitBucket](https://bitbucket.org/MarioP-Dev/)
[Phone](tel:+34633346053)
### Other projects
[QueMeVendes.es, un marketplace diferente](https://quemevendes.es)

[Alojamientos La Xiarapina](http://xiarapina.com)

