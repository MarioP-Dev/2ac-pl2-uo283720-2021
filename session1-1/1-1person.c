#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};

typedef struct _Person Person;
typedef Person* PPerson;

int main(int argc, char* argv[])
{
    Person Peter;
    PPerson pPeter;
    pPeter = &Peter;
    strcpy(Peter.name, "Peter");
    Peter.heightcm = 175;
    Peter.weightkg = 78.7;
   
    printf("%s's height: %dcm; %s's weight: %fkg\n", pPeter->name, pPeter->heightcm, pPeter->name, pPeter->weightkg);
    return 0;
}
